# Hangman's game ("Jeu du pendu" in french)

Find the secret word.

## Clone the repository:
```
$ git clone https://gitlab.gnome.org/allomix302/pendu.git
```

## Dependencies:
 - needs meson

## Install:
```
$ cd pendu
$ cmake .
$ make
$ make install
```

Good Game!
