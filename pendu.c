#include "functions.h"

char StartPendu(char niveau)
{
    // Variables
    char nmbCoups = 10, lettreFormatee = 0, motFormate[TAILLEMAX] = {0}, caracteresUtilises[26] = {'\0'}, lettresTrouvees[TAILLEMAX] = {0}, compte = 0;
    wchar_t mot[TAILLEMAX] = {0}, lettreProposee = 0, motReconstitue[TAILLEMAX] = {0};
    FILE *fichier = NULL;

    // Initialiser le random
    srand(time(NULL));

    // Mot pioché au hasard
    fichier = fopen(fichiermots, "r");
    if(fichier == NULL)
    {
      wprintf (L"\nVous ne possédez pas de fichier .dic ou le chemin est mal indiqué.\n"
L"Vérifiez que vous avez bien un fichier .dic de votre langue installé et spécifiez son emplacement dans le header functions.h.");
      EXIT_FAILURE;
    }
    motRandom(fichier, mot);
    fclose(fichier);
    simplifierMot (mot,motFormate);

    // On initialise le mot à reconstituer et le niveau

    nmbCoups = (wcslen(mot) + 10) / (niveau+1);

    // Boucle principale
    while (nmbCoups > 0)
    {
        wprintf(L"\f");
        // Récupérer l'entrée utilisateur
        wprintf(L"\n\nIl vous reste %d coups à jouer.\n", nmbCoups);
        wprintf(L"Donnez une lettre : ");
        lettreProposee = lireCaractere('l');
        lettreFormatee = simplifierLettre (lettreProposee);

        // On vérifie que la lettre n'a pas déjà été utilisée
        if(lettreDansMot(caracteresUtilises, lettreFormatee))
        {
            wprintf(L"Attention, vous avez déjà utlisé cette lettre.");
            continue;
        }


        // Ajout aux lettre utilisées
        addToList(caracteresUtilises, lettreFormatee);

        // On vérifie si elle se trouve dans le mot et dans ce cas on complète les trous
        if(lettreDansMot(motFormate, lettreFormatee))
        {
            wprintf(L"Vous avez vu juste, Le caractère '%c' se trouve dans le mot.\n", lettreFormatee);
            for (size_t i = 0; i < wcslen (mot); i++)
              {
                  if(motFormate[i] == lettreFormatee)
                  {
                      lettresTrouvees[i] = 1;
                  }
              }
        }
        else
        {
            nmbCoups--;
            wprintf(L"Désolé, le mot ne contient pas la lettre '%c'.\n", lettreFormatee);
        }
        // On affiche l'avancée ou si le mot est complétement retrouvé on donne la victoire
        compte = 0;
        for (size_t i = 0; i < wcslen (mot); i++)
          {
                compte += lettresTrouvees[i];
          }
        if(compte == wcslen (mot))
        {
            wprintf(L"\n");
            switch (niveau)
            {
            case 0:
                wprintf(L"Bravo !! C'est un bon début.");
                break;

            case 1:
                wprintf(L"Mes félicitations, tu prends du galon !!!");
                break;
            case 2:
                wprintf(L"Mais nous avons un champion !!! Un grand Bravo !!!!!");
                break;
            case 3:
                wprintf(L"Un grand maitre tu es. La sagesse en toi à jamais. Bravissimo !!!!!!");
                break;
            }

            wprintf(L"\nVous avez trouvé le mot : %ls.\n", mot);
            return 1;
        }
        else
        {
            reconstituerMot (mot, lettresTrouvees, motReconstitue);
            wprintf(L"Le mot secret : %ls \nVous avez déjà utilisé ces lettres: ", motReconstitue);
            for (size_t i = 0; i < strlen(caracteresUtilises); i++)
            {
                wprintf(L"'%c', ", caracteresUtilises[i]);
            }
            wprintf(L"\b\b.");
            if(nmbCoups == 0)
            {
                wprintf(L"\n\nMince, c'était la dernier tour.\nLe mot était %ls.\nTu as perdu.\n", mot);
            }
        }

    }
    return 0;
}
