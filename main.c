#include "main.h"
#include "version.h"

void Help(char **options, char **description)
{
    wprintf(L"\nOptions:\n");
    for (size_t i = 0; i < LEN_OPTIONS; i++)
    {
        wprintf(L"\t%s : %s.\n", options[i], description[i]);
    }
}

void Version(void)
{
    wprintf(L"\n//// La version du jeu du pendu est: %d.%d !\n", Pendu_VERSION_MAJOR, Pendu_VERSION_MINOR);
}

void NewArg(char *arg, char *description, char ***liste)
{
    size_t counter = 0;
    while (liste[0][counter] != 0)
      {
        counter++;
      }
    liste[0][counter] = arg;
    liste[1][counter] = description;
}

int main(int argc, char** argv)
{
    if (setlocale(LC_ALL, "") == NULL)
      {
      perror("setlocale");
      return EXIT_FAILURE;
      }

    // OPTIONS
    char *description[LEN_OPTIONS] = {0};
    char *options[LEN_OPTIONS] = {0};
    char **arguments[2] = {options, description};
    NewArg("--help", "Affiche l'aide", arguments);
    NewArg("--version", "Affiche la version", arguments);
    NewArg("--level", "Entrer le niveau de 0 à 3.", arguments);

    char continuer = 0;
    char niveau = 0;
    char asTugagne = 0;

    // On affiche le message de bienvenue
    wprintf(WELCOME_MESSAGE);

    // On vérifie s'il y a des options

        // On lance le jeu

    if(argc > 1)
    {
        if(strcmp(argv[1], options[0]) == 0)
        {
            Help(options, description);
            return 0;
        }
        else if (strcmp(argv[1], options[1]) == 0)
        {
            Version();
            return 0;
        }
        else if (strcmp(argv[1], options[2]) == 0)
        {
            niveau = argv[2][0] - 48;
        }
        else{
            wprintf(L"\nCOMMANDE: pendu [options] [arguments].\nL'option '%s' n'est pas disponible. '--help' pour toutes les options.\n", argv[1]);
            return 1;
        }
    }
    else{
         wprintf(L"\nQuelle difficulté voulez-vous choisir (du plus facile 0 au plus dur 3) ? ");
         niveau = lireCaractere('n');
    }
    do{
        if(continuer)
            {
                if(asTugagne)
                    {
                        wprintf(L"Il y a peu, vous avez gagné.\n* Voulez vous monter en niveau (tapez 0),\n* Ou au contraire rester ou vous êtes (tapez 1).\n");
                        niveau += lireCaractere('n');
                    }
                else
                    {
                        wprintf(L"Il y a peu, vous avez perdu.\n* Voulez vous baisser votre niveau (tapez 1),\n* Ou au contraire rester ou vous êtes (tapez 0).\n");
                        niveau -= lireCaractere('n');
                    }
            }
        asTugagne = StartPendu(niveau);
        wprintf(L"\n\n////// Rejouer? [0 = quitter, 1 rejouer] ");
        continuer = lireCaractere('n');
    } while (continuer);
    wprintf(L"\nBon vent !\n");
    return 0;
}
