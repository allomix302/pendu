#include "functions.h"

wchar_t lireCaractere(char type)
{
    wchar_t caractere = 0;
    if(type == 'n')
    {
        wscanf(L"%d", &caractere);
    }
    else
    {
        caractere = getwchar();
        caractere = towlower(caractere);
    }
    while (getwchar() != L'\n');
    return caractere;
}



long numRandom(long MIN, long MAX)
{
    long nombreRandom = 0;
    nombreRandom = (rand() % (MAX - MIN + 1)) + MIN;
    return nombreRandom;
}

void motRandom(FILE *fichier, wchar_t* mot)
{
    wchar_t caractere;
    long lignes = FILENMBLIGNES - FILESTARTSKIP - FILEENDSKIP;
    long randomNumber = numRandom(0, lignes - 1);
    lignes = 0;
    while (lignes < (FILESTARTSKIP + randomNumber))
    {
        caractere = fgetwc(fichier);
        if(caractere == L'\n')
        {
            lignes++;
        }
    }
    unsigned char compte;
    do
    {
        compte = 0;
        caractere = 0;
        do
        {
            caractere = fgetwc(fichier);
            mot[compte] = caractere;
            compte++;
        } while(caractere != L'/');
        mot[compte-1] = L'\0';
    } while (wcschr (mot, L'-'));
}

void addToList(char *liste, char element)
{
    unsigned char taille = strlen(liste);
    liste[taille] = element;
    liste[taille+1] = '\0';
}

/* void StatsFile(FILE* fichier, long *result) */
/* { */
/*   long lignes = 0; */
/*   wchar_t caractere[TAILLEMAX] = {0}; */
/*   wchar_t status = 0; */
/*   while (status != EOF) */
/*     { */
/*       status = fgetws (caractere, TAILLEMAX, fichier); */
/*       if(caractere[0]) */
/*       lignes++; */
/*     } */
/* } */

_Bool lettreDansMot(char *liste, char lettre)
{
    char *verif = NULL;
    verif = strchr(liste, lettre);
    if(verif != NULL)
    {
        return 1;
    }
    else
    {
        return 0;
    }
}

char simplifierLettre(wchar_t lettre)
{
    switch (lettre)
      {
      case L'é':
        lettre = L'e';
        break;
      case L'à':
        lettre = L'a';
        break;
      case L'è':
        lettre = L'e';
        break;
      case L'ê':
        lettre = L'e';
        break;
      case L'ù':
        lettre = L'u';
        break;
      case L'ç':
        lettre = L'c';
        break;
      case L'ï':
        lettre = L'i';
        break;
      case L'ü':
        lettre = L'u';
        break;
      }
    return lettre;
}

void simplifierMot(const wchar_t mot[], char motASimp[])
{
    wchar_t nouvMot[TAILLEMAX] = {0};
    wcscpy (nouvMot, mot);
    for (size_t i = 0; i < wcslen(mot); i++)
      {
          nouvMot[i] = simplifierLettre (nouvMot[i]);
      }
    wcstombs(motASimp, nouvMot, TAILLEMAX);
}

void reconstituerMot(const wchar_t mot[], const char lettresTrouvees[], wchar_t *nouvMot)
{
    wcscpy (nouvMot, mot);
    for (size_t i = 0; i < wcslen (nouvMot); i++)
      {
          if(! lettresTrouvees[i]) nouvMot[i] = L'*';
      }
}
