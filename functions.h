#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <string.h>
#include <wchar.h>
#include <locale.h>
#include <stddef.h>
#include <wctype.h>


#define fichiermots ".\\dicts\\fr.dic"
#define TAILLEMAX 20
#define FILESTARTSKIP 10666
#define FILEENDSKIP 32
#define FILENMBLIGNES 84140


long numRandom(long debut, long fin);

void motRandom(FILE *fichier, wchar_t* mot);

wchar_t lireCaractere(char type);

void addToList(char *liste, char element);

_Bool lettreDansMot(char *liste, char lettre);

void simplifierMot(const wchar_t mot[], char motASimp[]);

char simplifierLettre(wchar_t lettre);

void reconstituerMot(const wchar_t mot[], const char lettresTrouvees[], wchar_t *nouvMot);
