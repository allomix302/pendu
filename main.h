#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <wchar.h>

#include <locale.h>
#include <stddef.h>
#include <ctype.h>

#define LEN_OPTIONS 3
#define WELCOME_MESSAGE L"Bienvenue dans le jeu du pendu !\n\n"\
    L"Les règles:\n\t- Les caractères spéciaux comme 'é', 'à' ou 'ç'... sont convertis vers leur équivalent 'e', 'a', 'c'."\
    L"\n\t- N'entrez donc que des lettres de l'alphabet."\
    L"\n\t- Les mots composés avec '-' sont bannis."\
    L"\n\t- Bon jeu !\n"

char StartPendu(char niveau);

wchar_t lireCaractere(char type);
